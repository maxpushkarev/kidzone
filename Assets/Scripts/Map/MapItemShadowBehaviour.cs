﻿using UnityEngine;
using System.Collections;

public class MapItemShadowBehaviour : MonoBehaviour {

	[SerializeField]
	private bool useSwitcher = true;

	private void Awake()
	{
		Renderer[] items = GetComponentsInChildren<Renderer>();
		foreach (Renderer item in items) {

			item.shadowCastingMode = useSwitcher ? UnityEngine.Rendering.ShadowCastingMode.On 
				: UnityEngine.Rendering.ShadowCastingMode.Off;

			item.receiveShadows = item.gameObject.GetComponent<MapItemShadowReceiverComponent>() != null;

			if(useSwitcher)
			{
				item.gameObject.AddComponent<MapItemShadowSwitcher>();
			}
		}
	}
}