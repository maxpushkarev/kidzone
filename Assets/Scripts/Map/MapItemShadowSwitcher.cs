﻿using UnityEngine;
using System.Collections;

public class MapItemShadowSwitcher : MonoBehaviour {

	[SerializeField]
	private float checkingDelaySec = 0.5f;
	private Renderer itemRenderer;
	private IEnumerator coroutine;

	private void OnEnable()
	{
		this.itemRenderer = GetComponent<Renderer>();

		coroutine = WaitAndPrint(checkingDelaySec);
		StartCoroutine(coroutine);
	}

	public IEnumerator WaitAndPrint(float waitTime) {
		while (true) {

			yield return new WaitForSeconds(waitTime);

			if(!itemRenderer.isVisible)
			{
				itemRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			}

			StopCoroutine(coroutine);
		}
	}

	private void OnBecameVisible () {
		this.itemRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
	}

	private void OnBecameInvisible () {	
		this.itemRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
	}
}