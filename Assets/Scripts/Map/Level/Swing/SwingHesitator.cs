﻿using UnityEngine;

public class SwingHesitator : MonoBehaviour
{
	[SerializeField]
	private float w0 = 30;
    [SerializeField]
    private float amp = 40;

	private float t;
    private Vector3 initialEuler;
    private float targetAngle;

	private void OnEnable()
	{
        initialEuler = transform.eulerAngles;
        targetAngle = initialEuler.x;
        t = 0;
	}

	private void Update()
	{
		float dt = Time.deltaTime;
		t += dt;
        float phase = Mathf.Sin(w0 * t);
        float angle = amp * phase;
        transform.eulerAngles = new Vector3(angle + targetAngle, initialEuler.y, initialEuler.z);
	}

}
