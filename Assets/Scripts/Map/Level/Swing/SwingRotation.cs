﻿using UnityEngine;

public class SwingRotation : MonoBehaviour {

	[SerializeField]
	[Range(-360.0f, 360.0f)]
	private float rotateSpeedDeg = 20f;

	private void Update()
	{
		transform.Rotate(0, 0, rotateSpeedDeg * Time.deltaTime);
	}
}