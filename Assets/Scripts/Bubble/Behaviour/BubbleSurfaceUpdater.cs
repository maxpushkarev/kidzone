﻿using UnityEngine;

public class BubbleSurfaceUpdater : MonoBehaviour {

	private static readonly string DISPLACEMENT_NAME = "_WaveAxisDisplacement";
	private static readonly string SPEED_NAME = "_WaveAxisSpeed";
	private static readonly string ALPHA_NAME = "_OutputAlpha";

	private Material bubbleMat;
	private Vector4 targetDisplacement;

	[SerializeField]
	private float maxWaveAxisDisplacement = 0.25f;
	[SerializeField]
	private float maxAxisSpeed = 2.5f;
	[SerializeField]
	private float alphaSpeed = 0.5f;

	private int displacementID;
	private int speedID;
	private int alphaID;

	private float currentAlpha;
	private float CurrentAlpha
	{
		get{
			return currentAlpha;
		}
		set {
			currentAlpha = value;
			bubbleMat.SetFloat(alphaID, Mathf.Clamp01(currentAlpha));
		}
	}
	
	private void Awake()
	{
		displacementID = Shader.PropertyToID(DISPLACEMENT_NAME);
		speedID = Shader.PropertyToID(SPEED_NAME);
		alphaID = Shader.PropertyToID(ALPHA_NAME);

		bubbleMat = this.gameObject.GetComponent<Renderer>().material;
	}

	private void OnEnable()
	{	
		Vector4 targetDisplacement = CalculateTargetVector(maxWaveAxisDisplacement);
		Vector4 targetSpeed = CalculateTargetVector(maxAxisSpeed);

		bubbleMat.SetVector(displacementID, targetDisplacement);
		bubbleMat.SetVector(speedID, targetSpeed);

		CurrentAlpha = 0.0f;
	}

	private void Update()
	{
		float dt = Time.deltaTime;

		CurrentAlpha += alphaSpeed * dt;
		bool alphaReady = CurrentAlpha >= 1.0f;

		if (alphaReady)
		{
			enabled = false;
		}
	}

	private Vector4 CalculateTargetVector(float maxVal)
	{
		float x = Random.Range(0.0f, maxVal);
		float y = Random.Range(0.0f, maxVal);
		float z = Random.Range(0.0f, maxVal);

		Vector4 targetVec = new Vector4(x,y,z, 1.0f);
		return targetVec;
	}

	public void SetDefaultParams()
	{
		if (bubbleMat == null)
		{
			return;
		}

		CurrentAlpha = 0.0f;
	}

}