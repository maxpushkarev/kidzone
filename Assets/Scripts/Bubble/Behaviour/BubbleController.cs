﻿using UnityEngine;

public class BubbleController : MonoBehaviour {

	[SerializeField]
	private BubbleManager bubbleManager;
	[SerializeField]
	private float downSpeed = 2;
	[SerializeField]
	private BubbleBlowBehavior blow;

	public AudioSource source;

	private Transform spawn;
	private Vector3 downDir;

	private BubbleSurfaceUpdater bubbleSurfaceUpdater;
	private BubbleSurfaceUpdater BubbleSurfaceUpdater
	{
		get
		{
			if (bubbleSurfaceUpdater == null)
			{
				bubbleSurfaceUpdater = GetComponentsInChildren<BubbleSurfaceUpdater>(true)[0];
			}

			return bubbleSurfaceUpdater;
		}
		set 
		{
			bubbleSurfaceUpdater = value; 
		}
	}

	private void Awake()
	{
		downDir = Vector3.down;
	}

	private void Update()
	{
		transform.position += downDir * downSpeed * Time.deltaTime;
	}

	public void StartBubble(Transform spawn)
	{
		this.spawn = spawn;
		transform.position = spawn.position;

		enabled = true;
		blow.gameObject.SetActive (false);
		BubbleSurfaceUpdater.enabled = true;
		BubbleSurfaceUpdater.gameObject.SetActive(true);
	}

	public void FinalizeCurrentBubbleSession()
	{
		BubbleSurfaceUpdater.gameObject.SetActive(false);
		enabled = false;

		blow.gameObject.SetActive (true);
	}

	public void ResetBubble()
	{
		BubbleSurfaceUpdater.SetDefaultParams();
	}

	public void ReturnBubble()
	{
		ResetBubble ();
		bubbleManager.ReturnBubble(spawn, this);
	}

}