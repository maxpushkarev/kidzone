﻿using UnityEngine;
using System.Collections;

namespace Bubble
{
	public class EnvironmentUpdater : MonoBehaviour {

		private static readonly string CAMERA_NAME = "CAMERA_BUBBLE";
		private static readonly string ENVIRONMENT_NAME = "_Environment";

		[SerializeField]
		private float intervalSec;
		[SerializeField]
		private int textureSize;
		[SerializeField]
		private int smoothEdgesPixel = 2;

		private Material bubbleMat;
		private Transform shootingTransform;
		private Cubemap env;

		private void Awake(){

			bubbleMat = this.gameObject.GetComponent<Renderer> ().material;
			shootingTransform = this.gameObject.transform.Find("env_pos_shooting");
			env = new Cubemap(textureSize, TextureFormat.ARGB32, false);
			env.SmoothEdges(smoothEdgesPixel);

			//StartCoroutine(UpdateEnvironmentMap());
			_UpdateEnvironmentMap ();
		}




		private IEnumerator UpdateEnvironmentMap() {

			while (true) {
				_UpdateEnvironmentMap ();
				yield return new WaitForSeconds(intervalSec);
			}

		}

		private void _UpdateEnvironmentMap()
		{
			GameObject cameraTemp = new GameObject(CAMERA_NAME);
			Camera cam = cameraTemp.AddComponent<Camera>();
			cameraTemp.transform.position = shootingTransform.position;
			cameraTemp.transform.rotation = Quaternion.identity;
			cam.RenderToCubemap(env);
			env.SmoothEdges(smoothEdgesPixel);
			GameObject.DestroyImmediate(cameraTemp);

			bubbleMat.SetTexture(ENVIRONMENT_NAME, env);
		}
	}
}
