using UnityEngine;

public class BubbleOrbit : MonoBehaviour
{
	[SerializeField]
	private float orbitSpeed;
	[SerializeField]
	private Transform orbitObject;
	public BubbleController bubbleController;

	private void Update()
	{
		transform.RotateAround (orbitObject.position, orbitObject.up, orbitSpeed * Time.deltaTime);
	}

	private void OnTriggerEnter(Collider collider)
	{
		bubbleController.FinalizeCurrentBubbleSession();
	}

	private void OnTriggerStay(Collider collider)
	{
		bubbleController.FinalizeCurrentBubbleSession();
	}
}