using UnityEngine;

public class BubbleBlowBehavior : MonoBehaviour
{
	[SerializeField]
	private Transform bubbleSphere;
	[SerializeField]
	private Renderer bubbleSphereRenderer;
	[SerializeField]
	private BubbleController bubbleController;
	[SerializeField]
	private float lifeTime = 0.5f;
	[SerializeField]
	private Renderer[] drops;

	private Transform cam;
	private Transform parent;
	private float timer;

	private void Awake()
	{
		cam = Camera.main.transform;
		parent = transform.parent;
		Material mat = bubbleSphereRenderer.material;

		int dropsCount = drops.Length;
		for (int i =0; i < dropsCount; i++) {
			drops [i].material = mat;
		}
	}

	private void OnEnable()
	{
		transform.position = bubbleSphere.position;
		timer = 0;
	}

	private void Update()
	{
				timer += Time.deltaTime;
				if (timer >= lifeTime) {
						bubbleController.ReturnBubble ();
						gameObject.SetActive (false);
						return;
				}
			
				Vector3 dir2Cam = Vector3.Normalize (cam.position - transform.position);
				transform.rotation = Quaternion.LookRotation (dir2Cam, parent.up);
	}
}