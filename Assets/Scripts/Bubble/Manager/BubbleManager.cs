﻿using System.Collections.Generic;
using UnityEngine;

public class BubbleManager : ModeManager
{
	private const int BUBBLE_MASK = 1 << Layers.BUBBLE;

	[SerializeField]
	private BubbleController[] bubbles;
	[SerializeField]
	private Transform[] spawnPoints;
	[SerializeField]
	private float spawnIntervalSec = 5;
	[SerializeField]
	private float bubbleExitDelay = -1f;
	[SerializeField]
	private float bubbleSwitchDelay = 0.5f;


	private float spawnTimer;
	private int bubblesRequiredCount;
	private int spawnPointsCount;
	private Queue<BubbleController> bubbleQueue;
	private List<Transform> accessibleSpawnPoints;

	public override void Init()
	{
		int bubblesCount = bubbles.Length;
		spawnPointsCount = spawnPoints.Length;
		bubblesRequiredCount = Mathf.Min(bubblesCount, spawnPointsCount);

		bubbleQueue = new Queue<BubbleController>();
		accessibleSpawnPoints = new List<Transform>();
	}

	public override void Exit(bool isForNextGame)
	{
		delayAfterExitMode = isForNextGame ? bubbleSwitchDelay : bubbleExitDelay;
		base.Exit(isForNextGame);
	}

	public override void FinalizeAfterDelay()
	{
		if (delayAfterExitMode < 0) {
			return;
		}
		base.FinalizeAfterDelay ();
	}

	public override void Launch()
	{
		bubbleQueue.Clear();
		for (int i = 0; i < bubblesRequiredCount; i++)
		{
			BubbleController bubble = bubbles[i];
			bubble.ResetBubble ();
			bubbleQueue.Enqueue(bubble);
		}

		accessibleSpawnPoints.Clear();
		for (int i = 0; i < spawnPointsCount; i++)
		{
			Transform spawn = spawnPoints[i];
			accessibleSpawnPoints.Add(spawn);
		}

		spawnTimer = 0;
	}

	private void OnDisable()
	{
		if (!isActiveManager) {
			return;
		}


		int bubblesCount = bubbles.Length;
		for (int i = 0; i < bubblesCount; i++)
		{
			BubbleController bubble = bubbles[i];
			bubble.FinalizeCurrentBubbleSession();
		}
	}

	private void Update()
	{
		bool isOverUI = EventSystem.IsPointerOverGameObject();

		if (Input.GetMouseButtonDown (PlayerController.INPUT_CONTROL_INDEX)) {
			if (!isOverUI) {
				Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (clickRay, out hit, PlayerController.PLAYER_RAYCAST_DISTANCE, BUBBLE_MASK))
				{
					BubbleController controller = hit.transform.gameObject.GetComponent<BubbleOrbit>().bubbleController;
					controller.FinalizeCurrentBubbleSession();
					controller.source.Play();
				}
			}
		}


		if (spawnTimer > 0)
		{
			spawnTimer -= Time.deltaTime;
			return;
		}

		TryToSpawnBubble();
	}

	private void TryToSpawnBubble()
	{
		int accessibleBubbleCount = bubbleQueue.Count;
		if (accessibleBubbleCount == 0)
		{
			return;
		}

		int accessibleSpawnPointCount = accessibleSpawnPoints.Count;
		if (accessibleSpawnPointCount == 0)
		{
			return;
		}

		int rndIndex = Random.Range(0, accessibleSpawnPointCount);
		Transform spawnPoinTransform = accessibleSpawnPoints[rndIndex];
		BubbleController bubble = bubbleQueue.Dequeue();

		SpawnBubble(spawnPoinTransform, bubble);
	}

	private void SpawnBubble(Transform spawnPoint, BubbleController bubble)
	{
		accessibleSpawnPoints.Remove(spawnPoint);
		bubble.StartBubble (spawnPoint);
		spawnTimer = spawnIntervalSec;
	}

	public void ReturnBubble(Transform spawnPoint, BubbleController bubble)
	{
		bubbleQueue.Enqueue (bubble);
		accessibleSpawnPoints.Add (spawnPoint);
	}

	public override void Spawn(Transform spawnPoint)
	{
		Vector3 pos = spawnPoint.position;
		Quaternion rotation = spawnPoint.rotation;
		
		CamPlace.position = pos;
		CamPlace.rotation = rotation;
	}
}
