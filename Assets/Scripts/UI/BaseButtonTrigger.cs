using UnityEngine;
using UnityEngine.UI;

public abstract class BaseButtonTrigger : MonoBehaviour
{
	private const string DISABLE_TRIGGER_NAME = "Disabled";

	[SerializeField]
	protected Animator animator;
	[SerializeField]
	protected Button btn;
	[SerializeField]
	protected PlayerController player;

	private int disableTriggerID;

	protected virtual void Awake()
	{
		disableTriggerID = Animator.StringToHash(DISABLE_TRIGGER_NAME);
		interpolationInteractable = true;
		uiLogicInteractable = false;
	}

	private bool interpolationInteractable;
	public bool InterpolationInteractable
	{
		set
		{
			interpolationInteractable = value;
			UpdateBtnInteractable ();
		}
	}

	private bool uiLogicInteractable;
	public bool UILogicInteractable
	{
		set 
		{
			uiLogicInteractable = value;
			UpdateBtnInteractable ();
		}
	}

	private void UpdateBtnInteractable()
	{
		bool currentBtnInteractable = btn.interactable;
		bool newBtnInteractable = uiLogicInteractable && interpolationInteractable;

		if (currentBtnInteractable == newBtnInteractable) {
			return;
		}

		if (newBtnInteractable)
		{
			animator.ResetTrigger(disableTriggerID);
		}

		btn.interactable = newBtnInteractable;
	}
}