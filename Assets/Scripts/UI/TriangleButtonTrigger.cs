using UnityEngine;
using UnityEngine.UI;

public class TriangleButtonTrigger : BaseButtonTrigger
{	
	[SerializeField]
	private Sprite startSprite;
	[SerializeField]
	private Sprite stopSprite;
	[SerializeField]
	private MoveButtonTrigger front;
	[SerializeField]
	private MoveButtonTrigger back;

	private Image img;
	private Button.ButtonClickedEvent clickEvt;

	protected override void Awake()
	{
		base.Awake ();
		UILogicInteractable = true;
		img = btn.image;
		clickEvt = btn.onClick;
	}

	public void Press()
	{
		clickEvt.Invoke();
	}

	public void SwitchSprite()
	{
		player.StartGame ();

		if (player.GameStarted) {
			img.overrideSprite = stopSprite;
			front.UpdateMoveButton ();
			back.UpdateMoveButton ();
		} else {
			img.overrideSprite = startSprite;
			front.Hide ();
			back.Hide ();
		}
	}
}