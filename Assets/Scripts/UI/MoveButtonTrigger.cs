using UnityEngine;
using UnityEngine.UI;

public class MoveButtonTrigger : BaseButtonTrigger
{
	[SerializeField]
	private MoveButtonTrigger another;
	[SerializeField]
	private bool isFront;

	protected override void Awake()
	{
		base.Awake ();
		Hide ();
	}


	public void Hide()
	{
		UILogicInteractable = false;
	}

	public void UpdateMoveButton()
	{
		UILogicInteractable = player.CheckMoving (isFront);
	}

	public void PlayNextGame()
	{
		player.PlayNextGame (isFront);
		UpdateMoveButton ();
		another.UpdateMoveButton ();
	}
}