using UnityEngine;
using System.Collections.Generic;

public class AirplaneTransformer : MonoBehaviour
{
	public static int AIRPLANE_MOVING_MASK = (1 << Layers.BORDER) | (1 << Layers.PLANE_BOX);
	private const float IDLE_RECEIVE_INPUT_TIMER = -1f;
	private const float IDLE_REMAINING_ANGLE_WAY = -1f;
	private const float MOVING_EPS = 0.001f;
	private const float MAX_DELTA_DISTANCE = 0.0f;
	private const string DEBUG_COLLISION_FORMAT = "IsOverhead = {0} ; PrevAngle = {1} ; CurrentAngle = {2}";


	[SerializeField]
	private PlayerController player;
	[SerializeField]
	private Renderer baseRenderer;
	[SerializeField]
	private Transform zRotatingRoot;
	[SerializeField]
	private float normalInterpolationSpeedDeg = 0.5f;
	[SerializeField]
	private float collisionInterpolationSpeedDeg = 2f;
	[SerializeField]
	private float maxVerticalAngle = 45;
	[SerializeField]
	private float inputRotationCoeff = 0.5f;
	[SerializeField]
	private float maxZAngle = 60f;
	[SerializeField]
	private float maxAngleDistance = 35f;
	[SerializeField] 
	private float normalMovingSpeed = 7.5f;
	[SerializeField]
	private float collisionMovingSpeed = 5.0f;
	[SerializeField]
	private float zNormalApproximationSpeed = 100f;
	[SerializeField]
	private float zCollisionApproximationSpeed = 200f;
	[SerializeField]
	private Transform collisionMovingTransform;
	[SerializeField]
	private AirplaneParticleController particleController;
	[SerializeField]
	private float activeReceiveTimerSec = 1f;
	[SerializeField]
	private float nearDeg = 1.0f;
	[SerializeField]
	private Transform[] pointForResolveCollisionLock;
	[SerializeField]
	private int chainCollisionResolveFrames = 10;
	[SerializeField]
	private int resolveOffsetIterations = 100;
	[SerializeField]
	private float collisionOffset = 0.01f;

	private Vector3 targetHorizontalDirection;
	private Vector3 targetFWD;
	private Vector3 realForwardDirection;
	private Vector3 ZEulerAngles;
	private float verticalAngle;
	private float radius;
	private float zAngle;
	private float interpolationSpeedDeg;
	private bool hasCollision;
	private float receiveInputTimer;
	private float zApproximationSpeed;
	private float movingSpeed;
	private float requiredAngleWay;
	private List<Transform> collisionChainGO;
	private Transform firstCollisionGO;
	private Transform lastCollisionGO;
	private int resolvePointCount;
	private int currentChainCollisionFrameCount;
	private bool needToExit;
    private float collisionWayByFrame;


	private bool isNormalMode;
	private bool IsNormalMode 
	{
		get
		{
			return isNormalMode;
		}
		set
		{
			isNormalMode = value;

			if (isNormalMode)
			{
				movingSpeed = normalMovingSpeed;
				interpolationSpeedDeg = normalInterpolationSpeedDeg;
				zApproximationSpeed = zNormalApproximationSpeed;
				receiveInputTimer = IDLE_RECEIVE_INPUT_TIMER;
				particleController.SetCollisionMode (false);
				requiredAngleWay = IDLE_REMAINING_ANGLE_WAY;
			} else
			{
				movingSpeed = collisionMovingSpeed;
				interpolationSpeedDeg = collisionInterpolationSpeedDeg;
				zApproximationSpeed = zCollisionApproximationSpeed;
				receiveInputTimer = activeReceiveTimerSec;
				particleController.SetCollisionMode (true);
				requiredAngleWay = Mathf.Abs(Vector3.Angle(realForwardDirection, targetFWD));
			}
		}
	}

	public float MaxVerticalAngle
	{
		get { return maxVerticalAngle; }
	}

	public float CurrentVerticalAngle
	{
		get
		{
			Vector3 axis = transform.right;
			Vector3 projectedRealDirection = Vector3.ProjectOnPlane(realForwardDirection, Vector3.up);
			return RotateBehaviour.CalculateSignedAngle(
				projectedRealDirection,
				realForwardDirection,
				axis
			);
		}
	}

	private void Awake()
	{
		enabled = false;
	}

	public void Init()
	{
		Bounds bounds = baseRenderer.bounds;
		Vector3 size = bounds.size;
		radius = size.x * 0.5f;

		particleController.Init ();

		transform.parent = collisionMovingTransform.parent;
		collisionChainGO = new List<Transform> ();

		resolvePointCount = (pointForResolveCollisionLock == null) ? -1: pointForResolveCollisionLock.Length;
		if (resolvePointCount == 0) {
			resolvePointCount = -1;
		}

		needToExit = false;
	}

	public void Spawn(Transform spawnPoint)
	{
		Vector3 spawnPos = spawnPoint.position;
		Quaternion spawnRot = spawnPoint.rotation;

		collisionMovingTransform.position = spawnPos;
		collisionMovingTransform.rotation = spawnRot;

		transform.position = spawnPos;
		transform.rotation = spawnRot;
	}

	private void ModifyDirectionsAndAngle(Vector3 possibleFWD)
	{
		possibleFWD = possibleFWD.normalized;
		Vector3 possibleHorizontal = Vector3.ProjectOnPlane(possibleFWD, Vector3.up).normalized;
		collisionMovingTransform.rotation = Quaternion.LookRotation(possibleFWD);

		Vector3 axis = collisionMovingTransform.right;
		float angle = RotateBehaviour.CalculateSignedAngle(
			possibleHorizontal,
			possibleFWD,
			axis
		);

		bool isVerticalOverhead = (Mathf.Abs(angle) > maxVerticalAngle);
		if (isVerticalOverhead)
		{
			float clampedAngle = (angle > 0) ? maxVerticalAngle : -maxVerticalAngle;
			verticalAngle = clampedAngle;
			possibleFWD = Quaternion.AngleAxis(clampedAngle, axis) * possibleHorizontal;
		}
		else
		{
			verticalAngle = angle;
		}

		targetFWD = possibleFWD;
		targetHorizontalDirection = possibleHorizontal;

		collisionMovingTransform.rotation = Quaternion.LookRotation(targetFWD);
	}

	private void ResetCollisionParameters()
	{
		IsNormalMode = true;
		verticalAngle = 0;

		hasCollision = false;

		firstCollisionGO = null;
		lastCollisionGO = null;

		collisionChainGO.Clear();
		currentChainCollisionFrameCount = 0;
        collisionWayByFrame = 0;
    }

	public void SwitchOn()
	{
		ResetCollisionParameters();
		Vector3 possibleFWD = transform.forward;
		ModifyDirectionsAndAngle(possibleFWD);

		realForwardDirection = targetFWD;
		transform.rotation = Quaternion.LookRotation(realForwardDirection);

		zAngle = 0;
		zRotatingRoot.localPosition = Vector3.zero;
		ZEulerAngles = zRotatingRoot.localEulerAngles;
		Switch (true);
	}

	public void SwitchOff()
	{
		Switch (false);
	}

	private void Switch(bool enableValue)
	{
		gameObject.SetActive (enableValue);
		enabled = enableValue;
	}

	public void ApplyInputRotation(Vector3 delta)
	{
		float dy = delta.y;
		float vertDeltaAngle = - Mathf.Atan (dy / radius) * inputRotationCoeff;
		verticalAngle += vertDeltaAngle;
		verticalAngle = Mathf.Clamp (verticalAngle, -maxVerticalAngle, maxVerticalAngle);

		float dx = delta.x;
		float horDeltaAngle = Mathf.Atan (dx / radius) * inputRotationCoeff;
		Quaternion deltaRot = Quaternion.AngleAxis (horDeltaAngle, Vector3.up);
		targetHorizontalDirection = deltaRot * targetHorizontalDirection;
        UpdateTargetFWDValue();
	}

	private void Update()
	{
		needToExit = false;
		float dt = Time.deltaTime;
		float movingDelta = movingSpeed*dt;

		Vector3 lastPos = transform.position;
		hasCollision = false;
        collisionWayByFrame = movingDelta;

		UpdateCollisionFirstAndLast ();
		CheckCollision();
		CheckCollisionLock ();
		UpdateInputStatus();
		ApplyFWD (dt);
		ApplyZRotation (dt);
		Apply2CollisionTransform(movingDelta, lastPos);
	}

	private void UpdateCollisionFirstAndLast()
	{
		int chainLength = collisionChainGO.Count;

		if (chainLength > 0) {
			firstCollisionGO = collisionChainGO [0];
			lastCollisionGO = collisionChainGO [chainLength - 1];
		} else {
			firstCollisionGO = null;
			lastCollisionGO = null;
		}

		collisionChainGO.Clear ();
	}

	private void CheckCollisionLock()
	{
		if (!hasCollision) {
			currentChainCollisionFrameCount = 0;
			return;
		}

		currentChainCollisionFrameCount++;
		if (currentChainCollisionFrameCount >= chainCollisionResolveFrames) {

			#if UNITY_EDITOR
				Debug.Log("collision chain exit by frames");
			#endif

			ResolveCollisionLock ();
			return;
		}

        int currentChainCount = collisionChainGO.Count;
        Transform currentFirst = collisionChainGO [0];
		Transform currentLast = collisionChainGO [currentChainCount - 1];

		if (currentFirst != firstCollisionGO) {
			return;
		}

		if (currentLast != lastCollisionGO) {
			return;
		}

		#if UNITY_EDITOR
			Debug.Log("collision lock exit by chain");
		#endif

		ResolveCollisionLock ();
	}
	
	private void ResolveCollisionLock()
	{
		if (resolvePointCount <= 0) {
			return;
		}

		int index = Random.Range (0, resolvePointCount);
		Vector3 center = pointForResolveCollisionLock [index].position;
		Vector3 dir2Resolve = center - collisionMovingTransform.position;

		ModifyDirectionsAndAngle(dir2Resolve);

		IsNormalMode = false;
	}


	private void UpdateInputStatus()
	{
		if (!hasCollision) {
			return;
		}

		if (IsNormalMode) 
		{
			IsNormalMode = false;
		}
	}

	private void CheckCollision()
	{
		Vector3 source = collisionMovingTransform.position;
		Vector3 targetPos = source + targetFWD * collisionWayByFrame;
		Vector3 vec = targetPos - source;

		RaycastHit hit;
		bool hasHit = Physics.Raycast (source, vec.normalized, out hit, vec.magnitude+collisionOffset, AIRPLANE_MOVING_MASK);
		
		if (hasHit)
		{
			Collide(hit);
		} else {
			collisionMovingTransform.position = targetPos;
            collisionWayByFrame = 0;
		}

		hasCollision |= hasHit;

		if (needToExit)
		{
			return;
		}

		
		if (collisionWayByFrame <= MOVING_EPS) {
			return;
		}

		CheckCollision ();
	}

	private void StopOnOffsetLock()
	{
		player.AutoPressTriangleButton();
		needToExit = true;
		enabled = false;
	}

	private Vector3 UpdateCollisionPosition(RaycastHit hit)
	{
		Vector3 nrm = hit.normal;
		Vector3 point = hit.point;
		Vector3 collisionPoint = point;
        collisionWayByFrame -= Vector3.Magnitude(collisionMovingTransform.position - point);

        int resolveIterations = 0;
		while (true)
		{
			Vector3 vec2Offset = nrm * collisionOffset;
			RaycastHit offsetHit;
			bool canMoveOffset = !Physics.Raycast(collisionPoint, nrm, out offsetHit, collisionOffset*2, AIRPLANE_MOVING_MASK);
			resolveIterations++;
            collisionWayByFrame -= collisionOffset;
			if (canMoveOffset)
			{
				collisionPoint += vec2Offset;

				#if UNITY_EDITOR
					if (resolveIterations > 1)
					{
						Debug.Log("offset iterations count = " + resolveIterations);
					}
				#endif

				break;
			}
			else
			{
				if (resolveIterations >= resolveOffsetIterations)
				{
					StopOnOffsetLock();
					return Vector3.zero;
				}

				collisionPoint = offsetHit.point;
				nrm = offsetHit.normal;
			}
		}

		collisionMovingTransform.position = collisionPoint;

		return nrm;
	}

	private void Collide(RaycastHit hit)
	{
		Transform hitTransform = hit.transform;
		collisionChainGO.Add (hitTransform);
		float prevVerticalAngle = verticalAngle;

		Vector3 nrm = UpdateCollisionPosition(hit);
		
		Vector3 newDirection;
		newDirection = Vector3.ProjectOnPlane(
				targetFWD,
				nrm
		).normalized;

		Vector3 upVec = collisionMovingTransform.up;
		if (newDirection.magnitude == 0) {
			newDirection = Vector3.Cross (upVec, nrm).normalized;
		}

		Vector3 newDirectionHorizontal = Vector3.ProjectOnPlane (newDirection, Vector3.up).normalized;
		if (newDirectionHorizontal.magnitude == 0) {
			newDirectionHorizontal = Vector3.Cross (upVec, nrm).normalized;
		}

		float angle = RotateBehaviour.CalculateSignedAngle(
			newDirectionHorizontal, 
			newDirection,
			nrm
		);

		bool isVerticalOverhead = (Mathf.Abs (angle) > maxVerticalAngle);
		if (isVerticalOverhead) {
			float clampedAngle = (angle > 0) ? maxVerticalAngle : -maxVerticalAngle;
			newDirection = Quaternion.AngleAxis(clampedAngle, nrm) * newDirectionHorizontal;
		}

		collisionMovingTransform.rotation = Quaternion.LookRotation(newDirection);

		Vector3 axis = collisionMovingTransform.right;
		Vector3 projectedNewDirection = Vector3.ProjectOnPlane(newDirection, Vector3.up).normalized;
		verticalAngle = RotateBehaviour.CalculateSignedAngle(
			projectedNewDirection,
			newDirection,
			axis
		);

	#if UNITY_EDITOR

		if (prevVerticalAngle * verticalAngle < 0)
		{
			Vector3 collisionTransformPoint = collisionMovingTransform.position;
			PlayerController.DrawRay(collisionTransformPoint, targetFWD, Color.red);
			PlayerController.DrawRay(collisionTransformPoint, newDirectionHorizontal, Color.blue);
			PlayerController.DrawRay(collisionTransformPoint, newDirection, Color.green);
			Debug.Log(string.Format(DEBUG_COLLISION_FORMAT, isVerticalOverhead, prevVerticalAngle, verticalAngle));
		}

	#endif

		targetFWD = newDirection;
		targetHorizontalDirection = projectedNewDirection;
	}

	private void ApplyFWD(float dt)
	{
		float deltaMovingDirDeg = interpolationSpeedDeg * dt;
		float deltaMovingDirRad = deltaMovingDirDeg * Mathf.Deg2Rad;
		realForwardDirection =  Vector3.RotateTowards(realForwardDirection, targetFWD, deltaMovingDirRad, MAX_DELTA_DISTANCE);
		float angle2TargetFWD = Mathf.Abs(Vector3.Angle(realForwardDirection, targetFWD));

		if ((requiredAngleWay <= 0) || (angle2TargetFWD <= nearDeg))
		{
			if (receiveInputTimer <= 0) 
			{
				if (!IsNormalMode) 
				{
					IsNormalMode = true;
				}
			} else 
			{
				receiveInputTimer -= dt;
			}
		} 
		else 
		{
			requiredAngleWay -= deltaMovingDirDeg;
		}

		transform.rotation = Quaternion.LookRotation (realForwardDirection);
	}

	private void Apply2CollisionTransform(float movingDelta, Vector3 lastPos)
	{
		Vector3 currentPos = lastPos + realForwardDirection * movingDelta;
		Vector3 targetPos = collisionMovingTransform.position;

		float movingDelta2CollisionTransform = movingDelta * normalMovingSpeed / movingSpeed;
		transform.position = Vector3.MoveTowards(currentPos, targetPos, movingDelta2CollisionTransform);
	}

    private void UpdateTargetFWDValue()
    {
        collisionMovingTransform.rotation = Quaternion.LookRotation(targetHorizontalDirection);
        Vector3 rightAxis = collisionMovingTransform.right;
        targetFWD = Quaternion.AngleAxis(verticalAngle, rightAxis) * targetHorizontalDirection;
        collisionMovingTransform.rotation = Quaternion.LookRotation(targetFWD);
    }

	private void ApplyZRotation(float dt)
	{
		Vector3 projectedRealHorizontalDirection = Vector3.ProjectOnPlane(realForwardDirection, Vector3.up);
		float angleBetween = RotateBehaviour.CalculateSignedAngle(
			projectedRealHorizontalDirection, 
			targetHorizontalDirection,
			Vector3.up);

		float absAngleBetween = Mathf.Abs(angleBetween);
		float signAngleBetween = - Mathf.Sign(angleBetween);
		float k = Mathf.Clamp01(absAngleBetween/maxAngleDistance);
		float targetZAngle = Mathf.Lerp(0, maxZAngle, k)*signAngleBetween;
		zAngle = Mathf.MoveTowardsAngle (zAngle, targetZAngle, zApproximationSpeed * dt);
		ZEulerAngles.z = zAngle;
		zRotatingRoot.localEulerAngles = ZEulerAngles;
	}
}