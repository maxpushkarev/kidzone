﻿using UnityEngine;

public class AirplaneManager : ModeManager
{
	[SerializeField]
	private AirplaneSwitcher switcher;
	[SerializeField]
	private AirplaneTransformer transformer;

	private Vector2 prevMousePos;

	public override void Init()
	{
		switcher.Init();
		prevMousePos = Input.mousePosition;
	}

	public override void Launch()
	{
		switcher.SwitchOn();
	}

	private void Update()
	{
		Vector2 currentMousePos = Input.mousePosition;
		bool isOverUI = EventSystem.IsPointerOverGameObject();

		if (isOverUI)
		{
			return;
		}

		if (Input.GetMouseButtonDown (PlayerController.INPUT_CONTROL_INDEX)) {
			prevMousePos = currentMousePos;
		}

		Vector2 deltaMouse = currentMousePos - prevMousePos;

		if (Input.GetMouseButton (PlayerController.INPUT_CONTROL_INDEX)) {
			transformer.ApplyInputRotation (deltaMouse);
		}

		prevMousePos = currentMousePos;
	}

	private void OnDisable()
	{
		if (!isActiveManager)
		{
			return;
		}

		switcher.SwitchOff();
	}

	public override void Spawn(Transform spawnPoint)
	{
		switcher.Spawn (spawnPoint);
	}
}
