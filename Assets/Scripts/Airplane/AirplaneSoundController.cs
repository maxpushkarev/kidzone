﻿using UnityEngine;

public class AirplaneSoundController : MonoBehaviour
{
	[SerializeField]
	private AirplaneTransformer transformer;
	[SerializeField]
	private AudioSource engineSource;
	[SerializeField]
	private float fadeInTime = 2f;
	[SerializeField]
	private float fadeOutTime = 2f;
	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float downingSoundValue = 0.1f;

	private AirplaneSoundStates state;
	private float targetMaxVolume;
	private float fadeInSpeed;
	private float fadeOutSpeed;

	public void Init()
	{
		fadeInSpeed = 1/fadeInTime;
		fadeOutSpeed = -1/fadeOutTime;
		state = AirplaneSoundStates.IDLE;
		targetMaxVolume = 0;
		engineSource.volume = 0;
		enabled = false;
	}

	public void SwitchOn()
	{
		state = AirplaneSoundStates.FADE_IN;
		enabled = true;
	}

	public void SwitchOff()
	{
		state = AirplaneSoundStates.FADE_OUT;
	}

	private void Update()
	{
        float dt = Time.deltaTime;
        float maxAngle = transformer.MaxVerticalAngle;
		float angle = transformer.CurrentVerticalAngle;
		float kAngle = - Mathf.Clamp(angle/maxAngle, -1.0f, 1.0f);
        float kEngineVolume = kAngle * 0.5f + 0.5f;
		float engineVolume = Mathf.Lerp(downingSoundValue, 1, kEngineVolume);
		float resultEngineVolume = engineVolume*targetMaxVolume;
		UpdateVolume (engineSource, resultEngineVolume);

		if (state == AirplaneSoundStates.FADE_IN)
		{
			targetMaxVolume += fadeInSpeed*dt;
			if (targetMaxVolume >= 1)
			{
				targetMaxVolume = Mathf.Min(targetMaxVolume, 1);
				state = AirplaneSoundStates.IDLE;
			}
			return;
		}

		if (state == AirplaneSoundStates.FADE_OUT)
		{
			targetMaxVolume += fadeOutSpeed * dt;
			if (targetMaxVolume <= 0)
			{
				targetMaxVolume = Mathf.Max(targetMaxVolume, 0);
				state = AirplaneSoundStates.IDLE;
				enabled = false;
				engineSource.Pause();
			}
		}
	}

	private void UpdateVolume(AudioSource source, float volume)
	{
		source.volume = volume;
		bool isPlaying = source.isPlaying;

		if (volume == 0) {
			if (isPlaying) {
				source.Pause ();
			}
			return;
		}

		if (!isPlaying) {
			source.Play ();
		}
	}

	private enum AirplaneSoundStates
	{
		IDLE,
		FADE_IN,
		FADE_OUT
	}
}
