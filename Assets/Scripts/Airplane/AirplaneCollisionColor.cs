﻿using UnityEngine;

public class AirplaneCollisionColor : MonoBehaviour
{
	private const float COLLISION_ALPHA = 1.0f;

	[SerializeField]
	private AirplaneColorController airplaneColor;

	private float maxCollisionAlpha;
	public float MaxCollisionAlpha
	{
		get
		{
			return maxCollisionAlpha;
		}
		private set
		{
			maxCollisionAlpha = value;
			airplaneColor.UpdateAlpha();
		}
	}

	public void Init()
	{
		MaxCollisionAlpha = COLLISION_ALPHA;
	}
}
