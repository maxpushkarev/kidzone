using System;
using UnityEngine;

public class AirplaneParticleController : MonoBehaviour
{
	[SerializeField]
	private int requiredNonCollisionFrames = 5;
	[SerializeField]
	private ParticleSystem[] psArray;
	[SerializeField]
	private float startSpeedWorld = 2.5f;
	[SerializeField]
	private float startSpeedLocal = 5f;

	private int psCount;
	private int currentNonCollisionFrames;
	private ParticleSystemSimulationSpace currentSpace;

	public void Init()
	{
		psCount = psArray.Length;
		SetSimulationSpace (ParticleSystemSimulationSpace.World, startSpeedWorld);
		currentNonCollisionFrames = 0;
		enabled = false;
	}

	public void SwitchOn()
	{
		SwitchParticles(PlayParticleSystem);
	}

	public void SwitchOff()
	{
		SwitchParticles(StopParticleSystem);
	}

	public void SetCollisionMode(bool hasCollision)
	{
		if (hasCollision) {
			SetSimulationSpace (ParticleSystemSimulationSpace.Local, startSpeedLocal);
			return;
		}

		if (currentSpace == ParticleSystemSimulationSpace.World) {
			return;
		}

		if (enabled) {
			return;
		}

		enabled = true;
		currentNonCollisionFrames = 0;
	}

	private void Update()
	{
		if (currentNonCollisionFrames >= requiredNonCollisionFrames) {
			SetSimulationSpace (ParticleSystemSimulationSpace.World, startSpeedWorld);
			return;
		}

		currentNonCollisionFrames++;
	}

	private void SetSimulationSpace(ParticleSystemSimulationSpace space, float startSpeed)
	{
		enabled = false;

		if (currentSpace == space) {
			return;
		}
		currentSpace = space;

		for (int i = 0; i < psCount; i++) {
			ParticleSystem ps = psArray [i];
			ps.simulationSpace = space;
			ps.startSpeed = startSpeed;
		}
	}

	private void SwitchParticles(Action<ParticleSystem> actionToDoWithPS)
	{
		for (int i = 0; i < psCount; i++)
		{
			ParticleSystem ps = psArray[i];
			actionToDoWithPS(ps);
		}
	}

	private void PlayParticleSystem(ParticleSystem ps)
	{
		ps.Play();
	}

	private void StopParticleSystem(ParticleSystem ps)
	{
		ps.Stop();
	}
}