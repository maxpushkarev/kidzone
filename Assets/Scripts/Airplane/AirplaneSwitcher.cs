﻿using UnityEngine;

public class AirplaneSwitcher : MonoBehaviour
{
	[SerializeField]
	private AirplaneTransformer transformer;
	[SerializeField]
	private AirplaneColorController airplaneColor;
	[SerializeField]
	private AirplaneParticleController airplaneParticle;
	[SerializeField]
	private AirplaneSoundController airplaneSound;
	[SerializeField]
	private AirplaneCameraController airplaneCameraController;
	[SerializeField]
	private float fadeInTime = 1;
	[SerializeField]
	private float fadeOutTime = 1;

	private Transform camPlace;
	private float maxSwitchAlpha;
	private float fadeInSpeed;
	private float fadeOutSpeed;
	private float speed;

	private float Speed
	{
		get
		{
			return speed;
		}
		set
		{
			speed = value;
			gameObject.SetActive(true);
			enabled = true;
		}
	}

	public float MaxSwitchAlpha
	{
		get
		{
			return maxSwitchAlpha;
		}
		private set
		{
			maxSwitchAlpha = value;
			maxSwitchAlpha = Mathf.Clamp01(maxSwitchAlpha);
			airplaneColor.UpdateAlpha();
		}
	}

	public void Init()
	{
		airplaneCameraController.Init ();
		transformer.Init();
		airplaneColor.Init();
		airplaneSound.Init ();

		MaxSwitchAlpha = 0;
		fadeInSpeed = 1/fadeInTime;
		fadeOutSpeed = -1/fadeOutTime;
	}

	public void Spawn(Transform spawnPoint)
	{
		airplaneCameraController.ResetCameraParent ();
		transformer.Spawn (spawnPoint);
	}

	public void SwitchOn()
	{
		Speed = fadeInSpeed;
		airplaneParticle.SetCollisionMode (false);
		transformer.SwitchOn ();
		airplaneParticle.SwitchOn();
		airplaneSound.SwitchOn();
		airplaneCameraController.SwitchOn ();
	}

	public void SwitchOff()
	{
		Speed = fadeOutSpeed;
		airplaneParticle.SetCollisionMode (true);
		airplaneSound.SwitchOff();
	}

	private void Update()
	{
		MaxSwitchAlpha += Speed * Time.deltaTime;

		if (MaxSwitchAlpha >= 1)
		{
			enabled = false;
			return;
		}

		if (MaxSwitchAlpha <= 0)
		{
			enabled = false;
			airplaneCameraController.SwitchOff ();
			transformer.SwitchOff ();
			airplaneParticle.SwitchOff();
			gameObject.SetActive(false);
			return;
		}
	}
}
