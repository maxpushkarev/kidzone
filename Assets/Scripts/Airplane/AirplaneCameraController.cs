using UnityEngine;

public class AirplaneCameraController : MonoBehaviour
{
	private const float EPS_DISTANCE = 0.001f;
	private const float EPS_ANGLE = 1f;

	[SerializeField]
	private Transform camPlace;
	[SerializeField]
	private Transform cam;
	[SerializeField]
	private Camera camComponent;
	[SerializeField]
	private Transform firstCameraOrientation;
	[SerializeField]
	private Transform secondCameraOrientation;
	[SerializeField]
	private float degSpeed;
	[SerializeField]
	private float movingSpeed;
	[SerializeField]
	private float nearClippingTransition = 2f;
	[SerializeField]
	private float nearClippingStable = 0.3f;
	
	private Transform targetParentTransform;
	
	public void Init()
	{
	}

	public void ResetCameraParent()
	{
		targetParentTransform = firstCameraOrientation;
		camPlace.position = targetParentTransform.position;
		camPlace.rotation = targetParentTransform.rotation;
	}

	public void SwitchOn()
	{
		camComponent.nearClipPlane = nearClippingStable;
		enabled = true;
	}

	public void SwitchOff()
	{
		cam.parent = null;
		enabled = false;
	}

	private bool CheckVisibility()
	{
		Vector3 currentCameraPosition = firstCameraOrientation.position;
		Vector3 targetTransformPosition = transform.position;
		Vector3 vec = targetTransformPosition-currentCameraPosition;
		Vector3 vecNRM = vec.normalized;
		float vecLength = vec.magnitude;

		RaycastHit hit;

		if (Physics.Raycast (
			currentCameraPosition, 
			vecNRM, 
			out hit, 
			vecLength, 
			AirplaneTransformer.AIRPLANE_MOVING_MASK)) {
			return false;
		}

		if (Physics.Raycast (
			targetTransformPosition, 
			-vecNRM,
			out hit, 
			vecLength, 
			AirplaneTransformer.AIRPLANE_MOVING_MASK)) {
			return false;
		}

		return true;

	}


	private void Update()
	{
		bool isVisible = CheckVisibility ();

		targetParentTransform = isVisible ? firstCameraOrientation : secondCameraOrientation;
		float dt = Time.deltaTime;

		Vector3 targetPos = targetParentTransform.position;
		Quaternion targetRot = targetParentTransform.rotation;

		Vector3 newPos = Vector3.MoveTowards (camPlace.position, targetPos, movingSpeed * dt);
		Quaternion newRot = Quaternion.RotateTowards (camPlace.rotation, targetRot, degSpeed * dt);

		camPlace.position = newPos;
		camPlace.rotation = newRot;

		if (targetParentTransform == secondCameraOrientation) {
			camComponent.nearClipPlane = nearClippingTransition;
			return;
		}

		float dist = Vector3.Magnitude (targetPos - newPos);
		float angle = Quaternion.Angle (targetRot, newRot);

		if (dist > EPS_DISTANCE) {
			camComponent.nearClipPlane = nearClippingTransition;
			return;
		}

		if (angle > EPS_ANGLE) {
			camComponent.nearClipPlane = nearClippingTransition;
			return;
		}

		camComponent.nearClipPlane = nearClippingStable;

	}
}