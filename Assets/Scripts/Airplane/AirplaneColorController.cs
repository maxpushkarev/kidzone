﻿using UnityEngine;

public class AirplaneColorController : MonoBehaviour
{
	private const string OUTLINE_NAME = "_OutlineColor";
	private const string COLOR_NAME = "_Color";

	[SerializeField]
	private Material airplaneMaterialAsset;
	[SerializeField] 
	private Material smokeMaterialAsset;
	[SerializeField]
	private Renderer[] airplaneRenderers;
	[SerializeField]
	private ParticleSystemRenderer[] particleRenderers;
	[SerializeField]
	private float maxAirplaneAlpha = 1.0f;
	[SerializeField]
	private float maxAirplaneOutlineAlpha = 1.0f;
	[SerializeField]
	private float maxSmokeAlpha = 0.7f;
	[SerializeField]
	private float maxSmokeOutlineAlpha = 0.5f;
	[SerializeField]
	private AirplaneSwitcher switcher;
	[SerializeField]
	private AirplaneCollisionColor airplaneCollisionColor;

	private Material smokeMat;
	private Material airplaneMat;
	private int outlineKey;
	private int colorKey;

	public void Init()
	{
		smokeMat = Instantiate(smokeMaterialAsset);
		airplaneMat = Instantiate(airplaneMaterialAsset);

		outlineKey = Shader.PropertyToID(OUTLINE_NAME);
		colorKey = Shader.PropertyToID(COLOR_NAME);

		InitRenderers(smokeMat, particleRenderers);
		InitRenderers(airplaneMat, airplaneRenderers);

		airplaneCollisionColor.Init();
	}

	public void UpdateAlpha()
	{
		float maxSwitchAlpha = switcher.MaxSwitchAlpha;
		float maxCollisionAlpha = airplaneCollisionColor.MaxCollisionAlpha;
		float mergeCoeff = maxSwitchAlpha * maxCollisionAlpha;

		Color currentColor = airplaneMat.GetColor(colorKey);
		currentColor.a = maxAirplaneAlpha * mergeCoeff;

		Color currentOutline = airplaneMat.GetColor(outlineKey);
		currentOutline.a = maxAirplaneOutlineAlpha * maxSwitchAlpha;

		airplaneMat.SetColor(colorKey, currentColor);
		airplaneMat.SetColor(outlineKey, currentOutline);

		float smokeAlpha = maxSmokeAlpha * mergeCoeff;
		float smokeOutlineAlpha = maxSmokeOutlineAlpha * maxSwitchAlpha;

		Color currentSmokeColor = smokeMat.GetColor(colorKey);
		Color currentSmokeOutlineColor = smokeMat.GetColor(outlineKey);

		currentSmokeColor.a = smokeAlpha;
		currentSmokeOutlineColor.a = smokeOutlineAlpha;

		smokeMat.SetColor(colorKey, currentSmokeColor);
		smokeMat.SetColor(outlineKey, currentSmokeOutlineColor);
	}

	private void InitRenderers(Material mat, params Renderer[] renderers)
	{
		int renderersCount = renderers.Length;
		for (int i = 0; i < renderersCount; i++)
		{
			Renderer renderer = renderers[i];
			renderer.material = mat;
		}	
	}
}

