using UnityEngine;

public class PlayerInterpolator : MonoBehaviour
{
	[SerializeField]
	[Range(0.0f, 0.5f)]
	private float p1Lerp = 0.25f;
	[SerializeField]
	[Range(0.5f, 1.0f)]
	private float p2Lerp = 0.75f;
	[SerializeField]
	private float heightByMeter = 0.5f;
	[SerializeField]
	private float camSpeed = 60;
	[SerializeField]
	private Transform cam;
	[SerializeField]
	private PlayerDelayer delayer;
	[SerializeField]
	private PlayerController controller;
	[SerializeField]
	private BaseButtonTrigger[] triggers;
	[SerializeField]
	private float nearIntervalTime = 0.1f;

	private float camTimer;
	private float camInterval;

	private Quaternion initialCamRot;
	private Quaternion targetCameraRot;

	private Vector3 P0;
	private Vector3 P1;
	private Vector3 P2;
	private Vector3 P3;

	private bool needToEnable;
	private int triggersCount;
	private bool triggerState;

	public void SetTriggersState(bool state)
	{
		if (state == triggerState) {
			return;
		}

		triggerState = state;

		for (int i = 0; i < triggersCount; i++) {
			BaseButtonTrigger trigger = triggers [i];
			trigger.InterpolationInteractable = state;
		}
	}

	private void Awake()
	{
		enabled = false;
		triggerState = true;
		triggersCount = triggers.Length;
	}

	private void OnEnable()
	{
		if (!needToEnable) {
			enabled = false;
			SetTriggersState (true);
			return;
		}

		if (delayer.enabled) {
			enabled = false;
			return;
		}

		needToEnable = false;
		cam.parent = null;
	}

	public void Prepare(Transform targetCameraTransform)
	{
		targetCameraRot = targetCameraTransform.rotation;

		P0 = cam.position;
		P3 = targetCameraTransform.position;

		float length = Vector3.Magnitude (P3 - P0);
		camInterval = length / camSpeed;

		if (camInterval < nearIntervalTime)
		{
			FinalizeInterpolation();
			return;
		}

		float height = length * heightByMeter;

		P1 = Vector3.Lerp(P0, P3, p1Lerp);
		P2 = Vector3.Lerp(P0, P3, p2Lerp);

		P1.y += height;
		P2.y += height;

		initialCamRot = cam.rotation;

		camTimer = 0;
		SetTriggersState (false);
		needToEnable = true;
		enabled = true;
	}

	public void ApplyCameraMode(Transform parent)
	{
		cam.parent = parent;
		cam.localPosition = Vector3.zero;
		cam.localRotation = Quaternion.identity;
	}

	private void Update()
	{
		float dt = Time.deltaTime;
		camTimer += dt;

		if (camTimer >= camInterval)
		{
			FinalizeInterpolation();
			return;
		}

		float k = camTimer/camInterval;
		float k2 = k * k;
		float k3 = k2 * k;

		float oneMinusK = 1 - k;
		float oneMinusK2 = oneMinusK * oneMinusK;
		float oneMinusK3 = oneMinusK2 * oneMinusK;

		cam.position = oneMinusK3 * P0 + 3 * k * oneMinusK2 * P1
			+ 3 * k2 * oneMinusK * P2 + k3 * P3;

		cam.rotation = Quaternion.Slerp(initialCamRot, targetCameraRot, k);
	}

	private void FinalizeInterpolation()
	{
		cam.position = P3;
		cam.rotation = targetCameraRot;
		enabled = false;
		SetTriggersState(true);
		controller.PlayTargetGame();
	}
}