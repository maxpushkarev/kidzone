﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	public static readonly int INPUT_CONTROL_INDEX = 0;
	public static readonly float PLAYER_RAYCAST_DISTANCE = 1e6f;
	private const float DEBUG_RAY_LENGTH = 5f;
	private const float DEBUG_RAY_TIME = 0.5f;

	[SerializeField]
	private ModeManager[] games;
	[SerializeField]
	private SoundController backgroundSound;
	[SerializeField]
	private PlayerInterpolator playerInterpolator;
	[SerializeField]
	private TriangleButtonTrigger triangleButton;

	private Transform targetCameraTransform;
	private ModeManager targetModeManager;
	private int gameIndex;
	private int gameLastIndex;
	private bool gameStarted;

	public bool GameStarted {
		get {
			return this.gameStarted;
		}
	}

	private void Awake()
	{
		enabled = false;
		gameIndex = 0;
		int gamesLength = games.Length;
		gameLastIndex = gamesLength - 1;
		gameStarted = false;

		for (int i = 0; i < gamesLength; i++)
		{
			games[i].Activate();
		}
	}

	private void PrepareMode(
		ModeManager modeManager
	)
	{
		targetCameraTransform = modeManager.CamPlace;
		targetModeManager = modeManager;
		targetModeManager.SpawnMode();
		playerInterpolator.Prepare (targetCameraTransform);
	}


	private void StartMode(ModeManager modeManager, Transform parent)
	{
		playerInterpolator.ApplyCameraMode(parent);
		modeManager.gameObject.SetActive(true);
	}

	public void PlayTargetGame()
	{
		StartMode(targetModeManager, targetCameraTransform);
	}

	public void AutoPressTriangleButton()
	{
		triangleButton.Press();
	}

	public void StartGame()
	{
		if (!gameStarted)
		{
			PrepareMode(games[gameIndex]);	
			backgroundSound.FadeIn();
		}
		else
		{
			games [gameIndex].Exit (false);
			backgroundSound.FadeOut();
		}

		gameStarted = !gameStarted;
	}

	public bool CheckMoving(bool isFront)
	{
		if (isFront) {
			return gameIndex < gameLastIndex;
		} else {
			return gameIndex > 0;
		}
	}

	public void PlayNextGame(bool isFront)
	{
		games [gameIndex].Exit (true);

		if (isFront) {
			gameIndex++;
		} else {
			gameIndex--;
		}

		PrepareMode(games[gameIndex]);
	}

	public static void DrawRay(Vector3 pos, Vector3 dir, Color color, float time = DEBUG_RAY_TIME)
	{
		Debug.DrawRay (pos, dir.normalized * DEBUG_RAY_LENGTH, color, time);
	}
}