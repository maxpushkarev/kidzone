using UnityEngine;

public class PlayerDelayer : MonoBehaviour
{
	[SerializeField]
	private PlayerInterpolator interpolator;
	[SerializeField]
	private Transform cam;

	private float latencyTimer;
	private ModeManager modeManager;

	private void Awake()
	{
		latencyTimer = 0;
		enabled = false;
	}

	private void Update()
	{
		if (latencyTimer > 0) {
			latencyTimer -= Time.deltaTime;
			return;
		}

		FinalizeDelay();
	}

	private void FinalizeDelay()
	{
		cam.parent = null;
		enabled = false;
		modeManager.FinalizeAfterDelay ();
		interpolator.enabled = true;
	}

	public void Prepare(float latencyInterval, ModeManager modeManager)
	{
		latencyTimer = latencyInterval;
		this.modeManager = modeManager;

		if (latencyTimer <= 0)
		{
			FinalizeDelay();
			return;
		}

		interpolator.SetTriggersState (false);
		latencyTimer = latencyInterval;
		enabled = true;
	}
}