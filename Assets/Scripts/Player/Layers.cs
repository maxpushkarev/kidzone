public static class Layers
{
	public const int IGNORE_RAYCAST = 2;
	public const int UI = 5;
	public const int TERRAIN = 10;
	public const int BUBBLE = 12;
	public const int BORDER = 8;
	public const int PLANE_BOX = 13;
}