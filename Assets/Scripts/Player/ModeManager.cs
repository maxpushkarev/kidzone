using UnityEngine;
using UnityEngine.EventSystems;

public abstract class ModeManager : MonoBehaviour
{
	private int modeSpawnPointsCount;
	protected bool isActiveManager;

	[SerializeField]
	private Transform camPlace;
	[SerializeField]
	private EventSystem eventSystem;
	[SerializeField]
	protected float delayAfterExitMode;
	[SerializeField]
	protected PlayerDelayer delayer;
	[SerializeField]
	private Transform[] modeSpawnPoints;
	[SerializeField]
	private Transform cam;
	[SerializeField]
	private Camera cameraComponent;
	[SerializeField]
	private GameObject modeCamera;

	protected EventSystem EventSystem
	{
		get {return eventSystem;}
	}

	public Transform CamPlace
	{
		get {return camPlace;}
	}

	private void OnEnable()
	{
		Launch ();
	}

	private void OnApplicationQuit()
	{
		isActiveManager = false;
	}

	public virtual void Exit(bool isForNextGame)
	{
		if (!enabled) {
			return;
		}

		gameObject.SetActive (false);
		delayer.Prepare (delayAfterExitMode, this);
	}

	public virtual void FinalizeAfterDelay()
	{
		modeCamera.SetActive (false);
	}

	public void Activate()
	{
		isActiveManager = true;
		modeSpawnPointsCount = modeSpawnPoints.Length;
		Init();
	}

	public void SpawnMode()
	{
		Transform spawnPoint = modeSpawnPoints [0];
		Vector3 currentCamPosition = cam.position;
		float currentLength = Vector3.Magnitude (spawnPoint.position - currentCamPosition);

		for(int i = 0; i < modeSpawnPointsCount; i++)
		{
			Transform checkPoint = modeSpawnPoints [i];
			float checkLength = Vector3.Magnitude (checkPoint.position - currentCamPosition);
			if (checkLength > currentLength) {
				currentLength = checkLength;
				spawnPoint = checkPoint;
			}
		}

		modeCamera.SetActive (true);
		Spawn (spawnPoint);
	}

	public abstract void Spawn (Transform spawnPoint);
	public abstract void Init();
	public abstract void Launch();
}