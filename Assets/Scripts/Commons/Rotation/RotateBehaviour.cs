﻿using UnityEngine;

public class RotateBehaviour : MonoBehaviour {

	[SerializeField]
	private float rotateSpeedDeg = 20f;

	private void Update()
	{
		transform.Rotate (0, rotateSpeedDeg * Time.deltaTime, 0);
	}

	public static float CalculateSignedAngle(Vector3 v1, Vector3 v2, Vector3 n)
	{
		float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(v1, v2)));
		return sign * Vector3.Angle(v1, v2);
	}
}