﻿#define ZOFFSETWS
#define ZOFFSETSS
#include "./../Level/Outlining.cginc"

uniform half4 _WaveAxisDisplacement,
         	_WaveAxisSpeed;
uniform half _OutputAlpha;

half3 getVecNegativeX(half3 vec)
{
	return half3(-vec.x, vec.y, vec.z);
}

half4 calculateVertexModelPos(half4 inputVertex, half3 inputNormal) {
			
				half4 vertexModelPos = inputVertex;
     			half time = _Time.y;
     				
     			half3 wave = _WaveAxisDisplacement.xyz * abs(vertexModelPos.xyz);
				wave *=  half3(
									sin(time * _WaveAxisSpeed.x), 
									sin(time * _WaveAxisSpeed.y), 
									sin(time * _WaveAxisSpeed.z)
							 );
							 
				half3 displacement = inputNormal * wave;
				vertexModelPos.xyz += displacement;
				
				return vertexModelPos;
				//return inputVertex;
}