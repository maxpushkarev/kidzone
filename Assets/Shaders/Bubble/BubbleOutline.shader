﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Bubble/BubbleOutline" {

	Properties {
		_RefractionRatio ("Refraction Ratio", Range(0.5, 1.5)) = 1.02
		_FresnelBias ("Fresnel Bias", Range(0.0, 0.5)) = 0.1
		_FresnelPower ("Fresnel Power", Range(1.0, 4.0)) = 2.0
		_FresnelScale ("Fresnel Scale", Range(0.1, 2.5)) = 1.0
		_Environment ("Environment", CUBE) = "" {}
		
		_RScale ("Scale Of Red Channel", Range(0.0, 1.0)) = 1.0
		_GScale ("Scale Of Green Channel", Range(0.0, 1.0)) = 0.99
		_BScale ("Scale Of Blue Channel", Range(0.0, 1.0)) = 0.98
		
		_AlphaRefr ("Alpha Channel Of Refracion", Range(0.0, 1.0)) = 1.0
		_AlphaRefl ("Alpha Channel Of Reflection", Range(0.0, 1.0)) = 1.0
		
		_WaveAxisDisplacement ("Wave Axis Displacement", Vector) = (0.125, 0.3, 0.125, 1.0)
		_WaveAxisSpeed ("Wave Axis Speed", Vector) = (0.33, 0.005, 0.0025, 0.0)
		
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      	_RimPower ("Rim Power", Range(0.1,16.0)) = 3.0
      	
      	_SpecularColor ("Specular Color", color) = (1.0, 1.0, 1.0, 1.0)
		_SpecularPower ("Specular Power", Range(0.0, 10.0)) = 1.0
		
		_OutlineColor ("Outline Color", Color) = (0.0, 0.0, 0.0, 1.0)
		_Outline ("Outline width", Range (0.0, 0.03)) = 0.00

		_OutputAlpha ("Output Alpha", Range(0.0, 1.0)) = 1.0
		_ZOffsetScreenSpace ("Z Offset SS", Range (0.005, 0.5)) = 0.01
		_ZOffsetWorldSpace ("Z Offset WS", Range (0, 500)) = 75
	}

	SubShader {
		
		
		Tags 
   		{
   			"Queue" = "Transparent"
   			"RenderType"="Transparent"
   			"LightMode" = "ForwardBase"
   			"IgnoreProjector" = "True"
   		}

   		Blend SrcAlpha OneMinusSrcAlpha


		Pass
		{	
			Stencil {
				Ref 14
				Comp always
				Pass replace
			}

			ZWrite On
			ZTest LEqual
			Cull Back

			CGPROGRAM
			
				#include "Bubble.cginc"
	
				#define USE_RIM
 		 		#define USE_SPECULAR

 		 		#pragma target 3.0 
 		 		#pragma fragmentoption ARB_precision_hint_nicest
				#pragma glsl_no_auto_normalization
			  		
         		#pragma vertex vert  
         		#pragma fragment frag 
         		
         		uniform samplerCUBE _Environment;
         		uniform half _RefractionRatio, 
         					_FresnelBias, 
         					_FresnelPower, 
         					_FresnelScale;
         		
         		uniform half _RScale, 
         					 _GScale, 
         					 _BScale,
         					 _AlphaRefr,
         					 _AlphaRefl;
         		
         					  
         		uniform half4 _RimColor;
				uniform half _RimPower;
				
				uniform half4 _SpecularColor;
				uniform half _SpecularPower;
         		
				struct vertexInput {
     				half4 vertex : POSITION;
     				half3 normal : NORMAL;
				};
				
				 struct vertexOutput {
     				half4 pos : SV_POSITION;
     				half3 reflView : TEXCOORD0;
     				half3 refractViewR : TEXCOORD1;
     				half3 refractViewG : TEXCOORD2;
     				half3 refractViewB : TEXCOORD3;
     				half reflectionFactor : TEXCOORD4;
     				half3 normalWorld : TEXCOORD5;
     				half3 posWorld : TEXCOORD6;
				};
				
				
				vertexOutput vert(vertexInput input) 
				{
     				vertexOutput output;
					
					half4 vertexModelPos = calculateVertexModelPos(input.vertex, input.normal);
     				
     				half4 posWorld = mul(unity_ObjectToWorld, vertexModelPos);
     				half3 normalWorld = normalize(mul(half4(input.normal, 0.0), unity_WorldToObject).xyz);
     				half3 viewDir = calculateViewDir(posWorld.xyz);
     				half3 I = -viewDir;
     				half3 Inrm = normalize(I);
     				
     				output.pos = mul(UNITY_MATRIX_MVP, vertexModelPos);
     				output.posWorld = posWorld;
     				output.normalWorld = normalWorld;
     				output.reflView = reflect(I, normalWorld);
     				output.refractViewR = refract(Inrm, normalWorld, _RefractionRatio * _RScale);
     				output.refractViewG = refract(Inrm, normalWorld, _RefractionRatio * _GScale);
     				output.refractViewB = refract(Inrm, normalWorld, _RefractionRatio * _BScale);
     				output.reflectionFactor = _FresnelBias + _FresnelScale * pow(1.0 + dot(Inrm, normalWorld), _FresnelPower);
     				
     				return output;
     			}
     		
				
				half4 frag(vertexOutput input) : COLOR
				{
					half4 fragColor = half4(1.0, 1.0, 1.0, 1.0);
					
					half4 reflColor = texCUBE(_Environment, getVecNegativeX(input.reflView));
					reflColor.a = _AlphaRefl;
					
					half4 refrColor = half4(1.0, 1.0, 1.0, 1.0);
					
					refrColor.r = texCUBE(_Environment, getVecNegativeX(input.refractViewR)).r;
					refrColor.g = texCUBE(_Environment, getVecNegativeX(input.refractViewG)).g;
					refrColor.b = texCUBE(_Environment, getVecNegativeX(input.refractViewB)).b;
					refrColor.a = _AlphaRefr;
					
					fragColor = mix(refrColor, reflColor, clamp(input.reflectionFactor, 0.0, 1.0));
					half3 viewDir = calculateViewDir(input.posWorld.xyz);
					
					#ifdef USE_SPECULAR
						
						half3 lightDir;
						half atten;
						calculateLighting(input.posWorld, lightDir, atten);
						
						half4 actualSpec = calculateBlinnPhongPart(
														input.normalWorld, 
														lightDir, 
														viewDir, 
														_SpecularColor, 
														_SpecularPower, 
														atten
													);
						fragColor += actualSpec;
						
					#endif
					
					#ifdef USE_RIM
						half4 emissionPart = calculateRimLighting(input.normalWorld, viewDir, _RimColor, _RimPower);
						fragColor += emissionPart;
					#endif

					fragColor.a = max(0.0, min(1.0, fragColor.a));
					fragColor.a *= _OutputAlpha;

					return fragColor;
				}
				
				
			ENDCG
		}

		Pass
		{

			Stencil {
				Ref 14
				Comp always
				Pass zero
			}

   			ZWrite On
			ZTest LEqual
			Offset 1, 1
			Cull Front
			ColorMask RGB
			
			CGPROGRAM

			#include "Bubble.cginc"

			#pragma target 3.0 
			#pragma fragmentoption ARB_precision_hint_nicest
			#pragma glsl_no_auto_normalization

			#pragma vertex vert
			#pragma fragment frag
			
			vertexOutlineOutput vert(vertexOutlineInput input) {
			
				vertexOutlineOutput output;
				
				half4 vertexModelPos = calculateVertexModelPos(input.vertex, input.normal);

				output.pos = mul(unity_ObjectToWorld, vertexModelPos);
				output.pos = calculateOutlinePos(output.pos, input.normal);
				output.col = _OutlineColor;
			
				return output;
			}

			half4 frag(vertexOutlineOutput input) : COLOR
			{
				half4 resColor = input.col;
				resColor.a = _OutputAlpha;
				return resColor;
			}
			
			
			ENDCG
		}

	} 

	Fallback "Diffuse"
}