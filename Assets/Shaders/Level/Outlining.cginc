// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

#include "./../Commons/Commons.cginc"

struct vertexOutlineInput {
     half4 vertex : POSITION;
     half3 normal : NORMAL;
};
				
struct vertexOutlineOutput {
     half4 pos : SV_POSITION;
     #ifdef OUTLINE_CLIPPING
    	half4 posWorld : TEXCOORD0;
     #endif
     half4 col : COLOR;
};

half _ZOffsetWorldSpace;
half _Outline;
half4 _OutlineColor;
half _ZOffsetScreenSpace;
static const half clippingDist = 5;


half4 calculateOutlinePos(half4 pos, half3 nrm)
{
	#ifdef ZOFFSETWS
		half3 viewDir = calculateViewDir(pos.xyz);
		pos.xyzw -= half4(viewDir.x, viewDir.y, viewDir.z, 0.0)* _ZOffsetWorldSpace;
	#endif

	pos = mul(UNITY_MATRIX_VP, pos);
	half3 norm   = normalize(mul(UNITY_MATRIX_MV, half4(nrm.xyz, 0.0))).xyz;
	half2 _offset = normalize(TransformViewToProjection(norm.xy));

	#ifdef ZOFFSETSS
		pos.zw += half2(_ZOffsetScreenSpace, 0);	
	#endif

	pos.xy += _offset * max(0.0, pos.z)  * _Outline;

	return pos;
}		
			
vertexOutlineOutput vertOutline(vertexOutlineInput input) {
			
		vertexOutlineOutput output;
		half4 posWorld = mul(unity_ObjectToWorld, input.vertex);
		output.pos = calculateOutlinePos(posWorld, input.normal);
		#ifdef OUTLINE_CLIPPING
			output.posWorld = posWorld;
		#endif
		output.col = _OutlineColor;
		return output;
}


half4 fragOutline(vertexOutlineOutput input) : COLOR
{
	#ifdef OUTLINE_CLIPPING
		half3 posWorld = input.posWorld.xyz;
		clip(length(calculateViewVec(posWorld)) - clippingDist);
	#endif

	return input.col;
}