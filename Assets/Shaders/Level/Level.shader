﻿Shader "Map/Level" {

	Properties {
		_Color ("Main Color", Color) = (0.5, 0.5, 0.5 ,1.0)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_OutlineColor ("Outline Color", Color) = (0.0, 0.0, 0.0, 1.0)
		_Outline ("Outline width", Range (0.002, 0.03)) = 0.005
		_ZOffsetScreenSpace ("Z Offset SS", Range (0.005, 0.5)) = 0.01
		_ZOffsetWorldSpace ("Z Offset WS", Range (0.0, 500.0)) = 75.0
	}

	SubShader {

		Blend SrcAlpha OneMinusSrcAlpha

		Tags { 
			"Queue"="Geometry+80"
			"RenderType"="Transparent"
			"LightMode" = "ForwardBase" 
			"IgnoreProjector" = "True"
		}

		Pass
		{	
			Stencil {
				Ref 13
				Comp always
				Pass replace
			}

			Cull Back
			ZWrite On
			ZTest LEqual
			ColorMask RGB

			CGPROGRAM
			
				#include "Level.cginc"
				
 		 		#pragma target 3.0   
 		 		#pragma fragmentoption ARB_precision_hint_nicest	
 		 		#pragma glsl_no_auto_normalization	
         		#pragma vertex vert  
         		#pragma fragment frag		
				
			ENDCG
		}

		Pass
		{

			Stencil {
				Ref 13
				Comp notequal
				Pass zero
			}

			Cull Front
			ZTest Always
			ZWrite Off
			ColorMask RGB

			CGPROGRAM

			//#define ZOFFSETWS
			//#define OUTLINE_CLIPPING

			#include "Outlining.cginc"

			#pragma target 3.0 
			#pragma fragmentoption ARB_precision_hint_nicest
			#pragma glsl_no_auto_normalization
			#pragma vertex vertOutline
			#pragma fragment fragOutline
			
			ENDCG
		}
	}

	Fallback "Diffuse"
}