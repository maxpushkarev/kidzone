// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

#include "./../Commons/Commons.cginc"
#include "AutoLight.cginc"


sampler2D _MainTex;
half4 _Color;
	
struct vertexInput {
     half4 vertex : POSITION;
     half3 normal : NORMAL;
     half2 uv : TEXCOORD0;
     half4 color: COLOR;
};
				
struct vertexOutput {
     half4 pos : SV_POSITION;
     half2 uv : TEXCOORD0;
     half3 normalWorld : TEXCOORD1;
     half4 posWorld : TEXCOORD2;
     half4 color: COLOR;
     
     #ifdef SHADOWS
		 SHADOW_COORDS(3)
     #endif
};
				
				
vertexOutput vert(vertexInput v) 
{
     vertexOutput output;
     output.pos = mul(UNITY_MATRIX_MVP, v.vertex);
     output.posWorld = mul(unity_ObjectToWorld, v.vertex);
     output.normalWorld = normalize(mul(half4(v.normal, 0.0), unity_WorldToObject).xyz);
     output.color = v.color;
     output.uv = v.uv;
     
     #ifdef SHADOWS
		TRANSFER_SHADOW(output);
     #endif   
     
     return output;
}
				
half4 frag(vertexOutput input) : COLOR
{
	half4 mainColor = tex2D (_MainTex, input.uv) * _Color;
	half3 normalDirection = normalize(input.normalWorld);
					
	half3 lightDirection;
	half attenuation;
	
	half3 viewDirection = calculateViewDir(input.posWorld.xyz);
	calculateLighting(input.posWorld, lightDirection, attenuation);
	
	half4 ambientPart = calculateAmbientPart(mainColor);
	
	half4 diffusePart = _LightColor0 * mainColor;
	half NdotL = dot(normalDirection, viewDirection); 
					
	diffusePart *= max(0.0, NdotL) * attenuation;
	
	#ifdef SHADOWS
		half shadowAttenuation = SHADOW_ATTENUATION(input);	
		diffusePart *= shadowAttenuation;
	#endif
				
	#ifdef UNITY5
	
	#else
		diffusePart *= 2.0;	
	#endif
					
	half4 fragColor = diffusePart + ambientPart;
	fragColor.rgb *= input.color.rgb;
	fragColor.a = _Color.a;
					
	return fragColor;
}