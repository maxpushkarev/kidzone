# README #

### Demo: ###

[https://maxpushkarev.ru/demo?item=kidzone](https://maxpushkarev.ru/demo?item=kidzone)

![492d355d3f5dd258a84ca8c32b521bcf.png](https://bitbucket.org/repo/ojg5EX/images/1595945465-492d355d3f5dd258a84ca8c32b521bcf.png)

### What to improve? ###

* Adapt outline shaders to the 16bit depth buffer (test with Andriod 5.0.2)
* Adapt inputs for mobile browsers 
  (when Unity WebGL for mobile is supported http://docs.unity3d.com/Manual/webgl-browsercompatibility.html)